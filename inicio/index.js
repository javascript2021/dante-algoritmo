// console.log("hola programadores")
const notas = [
    {
        'id':1,
        'content':"titulo 1",
    },
    {
        'id':1,
        'content':"titulo 2",
    },
    {
        'id':1,
        'content':"titulo 3",
    },
    {
        'id':1,
        'content':"titulo 4",
    },
]
// vamos a solicitar con REQUIRE QUE NOS CARGUE EL MODO HTTP, PARA EMPEZAR A TRABAJAR CON EL SEVIDOR 
// const http = require ('http');
// // vamos a crear otra varible  para que , de la constante http , cree el servidor 
// const app = http.createServer((request, response) => {
//     response.writeHead(200, {'content-type':'application/json'})
//     // response.end('hola programadores, aprendiendo nodejs  ahora recarga solo')
//     response.end(JSON.stringify(notas))
// });
// vamos a crear un servidor con express
const express = require('express')
const app = express()

app.get('/', (request,response) => {
    response.send('<h1>PAGINA PRINCIPAL</h1>')
})


app.get('/notas/', (request, response) => {
    response.json(notas)
})

const port = 3001 
app.listen(port)
console.log(`Server is running on port ${port}`)
 