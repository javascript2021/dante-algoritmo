// console.log("esta funcionando, que tal ");
// // document.write("hola");
const express = require('express');
const app = express();
const port = 3000;
const path = require('path');   // PARA COMPATIBILIDAD CON BARRAS EN DIFERENTES S.O
app.listen(port);
console.log(`Server is running on port ${port}`);

app.get('/', (request, Response) =>{
    // Response.send('hola mundo');
    console.log(__dirname);
    console.log(path.join(__dirname, '/html/index.html'));
    Response.sendFile(path.join(__dirname,'/html/index.html'));
})
